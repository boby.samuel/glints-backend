const {
    getAllClassroomData  
} = require('../service/classrooms')

exports.getAllClassrooms = async (req, res, next) => {
    try {
        const classroomData = await getAllClassroomData()
        res.status(200).json({
            success: true,
            message: 'testing express success',
            data: classroomData
        })
    }
    catch(err) {
        console.log(err)
        res.status(400).json({
            success: false,
            message: 'express testing failed',
            data: 'failed',
            error: err
        })
    }
}