const { getAllSchedules } = require('../service/schedules')


exports.allSchedules = async (req, res, next) => {
    try {
        const data = await getAllSchedules()
        console.log(data)
        res.status(200).json({
            success: true,
            message: 'all schedules',
            data
        })
    }
    catch(err) {
        res.status(400).jon({
            success: false,
            message: 'err'
        })
    }
}