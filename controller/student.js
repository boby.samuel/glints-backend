const {
    getStudentData,
    getAllStudentData,
    addStudentData
} = require('../service/student')

const bcrypt = require('bcrypt')
exports.getStudent = async (req, res, next) => {
    try {
        const { id } = req.params
        const studentData = await getStudentData({ id })
        // console.log(Object.keys(res))
        res.status(200).json({
            success: true,
            message: 'testing express success',
            data: studentData
        })
    }
    catch(err) {
        res.status(400).json({
            success: false,
            message: 'express testing failed',
            data: 'failed',
            error: err
        })
    }
}

exports.getAllStudents = async (req, res, next) => {
    try {
        const studentData = await getAllStudentData()
        res.status(200).json({
            success: true,
            message: 'testing express success',
            data: studentData
        })
    }
    catch(err) {
        res.status(400).json({
            success: false,
            message: 'express testing failed',
            data: 'failed',
            error: err
        })
    }
}

exports.createStudent = async ( req, res, next) => {
    try {
        console.log(req.body)
        const { password } = req.body // assuming req.body has been validated

        // hash the password
        const hashedPassword = bcrypt.hashSync(password, 10)
        req.body.password = hashedPassword
        // store data
        console.log(req.body)
        const studentData = await addStudentData(req.body)
        res.status(200).json({
            success: true,
            message: 'testing express success',
            data: studentData
        })
    }
    catch(err) {
        console.log(err)
        res.status(400).json({
            success: false,
            message: 'express testing failed',
            data: 'failed',
            error: err
        })
    }
}