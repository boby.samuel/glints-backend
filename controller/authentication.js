const jwt = require('jsonwebtoken')
const bcrypt = require('bcrypt')

const { checkPassword } = require('../utils/helper')
const { getStudentData } = require('../service/student')

exports.login = async (req, res, next) => {
    try {
        const { name, password } = req.body
        // get user data
        const studentData = await getStudentData({ name })
        if (!studentData) {
            throw new Error('this student is not registered')
        }
        console.log(studentData)
        // compare passwords
        const passwordMatch = bcrypt.compareSync(password, studentData.password)
        if (!passwordMatch) {
            throw new Error('Wrong Password')
        }
        const payload = {
            id: studentData.id,
            name
        }
        const token = jwt.sign(payload, 'MY_SECRET', { expiresIn: '2h' })
        res.status(200).json({
            success: true,
            message: 'success login. user authenticated!',
            token
        })
    }
    catch(err) {
        res.status(400).json({
            success: false,
            message: err.message || 'failed'
        })
    }
}

exports.validateToken = async (req, res, next) => {
    try {
        // console.log(req)
        const { authorization } = req.headers
        console.log(authorization)
        console.log('before')
        const payload = jwt.verify(authorization, 'MY_SECRET')
        console.log(payload)
        next()
    }
    catch(err) {
        res.status(400).json({
            success: false,
            message: err.message
        })
    }
}