class UserError extends Error {
    constructor(message) {
        super(message);
        this.name = 'UserError';
    }
}

exports.idValidation = async (req, res, next) => {
    try {
        console.log('validation id')
        const { id } = req.query
        if (isNaN(id)) throw new UserError('numeric input expected')
        next()
    }
    catch(err) {
        next(err)
    }
}