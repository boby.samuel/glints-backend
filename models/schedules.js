'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class schedules extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      schedules.belongsTo(models.classroom, {
        foreignKey: 'classroom_id',
        as: 'classroom'
      })
      schedules.belongsTo(models.teacher, {
        foreignKey: 'teacher_id',
        as: 'teacher'
      })
      schedules.belongsTo(models.subject, {
        foreignKey: 'subject_id',
        as: 'subject'
      })
      schedules.belongsTo(models.Shift, {
        foreignKey: 'shift',
        as: 'sesi'
      })
    }
  };
  schedules.init({
    day: DataTypes.STRING,
    shift: DataTypes.INTEGER,
    classroom_id: DataTypes.INTEGER,
    teacher_id: DataTypes.INTEGER,
    subject_id: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'schedules',
  });
  return schedules;
};