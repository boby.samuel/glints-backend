'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class classroom extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      classroom.hasMany(models.student, {
        foreignKey: 'classroom_id',
        as: 'students'
      })
    }
  };
  classroom.init({
    name: DataTypes.STRING,
    homeroom: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'classroom',
  });
  return classroom;
};