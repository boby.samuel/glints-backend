const { 
    student,
    classroom,
    schedules,
    teacher
 } = require('../models')


exports.getStudentData = async query => {
    try {
        let studentData = await student.findOne({
            // attributes: ['name', 'password'],
            include: [
                {
                    model: classroom,
                    as: 'classroom'
                }
            ],
            where: query
        })
        return JSON.parse(JSON.stringify(studentData))
    }
    catch(err) {
        throw err
    }
}

exports.getAllStudentData = async () => {
    try {
        const result = await student.findAll({
            attributes: {
                exclude: ['createdAt', 'updatedAt']
            },
            include: [
                {
                    model: classroom,
                    as: 'classroom',
                    attributes: {
                        exclude: ['createdAt', 'updatedAt']
                    }
                }
            ]
        })
        return JSON.parse(JSON.stringify(result))
    }
    catch(err) {
        throw err
    }
}

exports.addStudentData = async data => {
    try {
        // assuming data is already validated
        await student.create(data)
    }
    catch(err) {
        throw err
    }
}