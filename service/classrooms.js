const { 
    student,
    classroom,
    schedules,
    teacher
 } = require('../models')

exports.getAllClassroomData = async query => {
    try {
        const result = await classroom.findAll({
            attributes: ['id', 'name'],
            include: [
                {
                    model: student,
                    as: 'students',
                    attributes: ['id', 'name']
                }
            ]
        })
        return JSON.parse(JSON.stringify(result))
    }
    catch(err) {
        throw err
    }
}