const { 
    student,
    classroom,
    schedules,
    teacher,
    subject,
    Shift
 } = require('../models')

 exports.getAllSchedules = async query => {
     try {
        const result = await schedules.findAll({
            attributes: ['id', 'day'],
            include: [
                {
                    attributes: ['id', 'name'],
                    model: classroom,
                    as: 'classroom',
                    include: [
                        {
                            model: student,
                            as: 'students',
                            attributes: ['id', 'name']
                        }
                    ]
                },
                {
                    attributes: ['id', 'name'],
                    model: teacher,
                    as: 'teacher'
                },
                {
                    attributes: ['id', 'name'],
                    model: subject,
                    as: 'subject'
                },
                {
                    attributes: ['id', 'start', 'end'],
                    model: Shift,
                    as: 'sesi'
                }
                
            ]
        })
        return JSON.parse(JSON.stringify(result))
     }  
     catch(err) {
         throw err
     }
 }


//  -- API /schedules?student_name=dallah
// -- requirements:
// if student name doesn't exist, return 400
return {
    data: [
        {
            id: 1,
            day: '2021-01-01',
            shift: {
                id: 1,
                start: '7am',
                end: '9am'
            },
            subject:{
                name: 'biology'
            },
            teacher: {
                name: 'susi'
            }
        },
        {
            id: 2,
            date: '2021-01-01',
            shift: {
                id: 2,
                start: '9am',
                end: '11am'
            },
            subject:{
                name: 'math'
            },
            teacher: {
                nmae: 'josh'
            }
        },
        {
            id: 3,
            date: 'string <2021-01-01>',
            shift: {
                id: 3
            },
            subject:{

            },
            teacher: {

            }
        }
    ]
}