const router = require('express').Router()
const { getStudent, getAllStudents, createStudent } = require('../controller/student')
const { idValidation } = require('../controller/validations')
const { validateToken } = require('../controller/authentication')

router.get('/', getAllStudents)
router.post('/', createStudent)

router.get('/:id', getStudent)

module.exports = router