const { login } = require('../controller/authentication')
const router = require('express').Router()

router.post('/login', login)

module.exports = router