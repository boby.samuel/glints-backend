const {allSchedules} = require('../controller/schedules')
const router = require('express').Router()

router.get('/', allSchedules)

module.exports = router