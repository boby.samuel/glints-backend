const express = require('express');
const router = express.Router();

const classRouter = require('./classrooms')
const scheduleRouter = require('./schedule')
const studentRouter = require('./student')
const authRouter = require('./authentication')
const { validateToken } = require('../controller/authentication')

router.use('/student', validateToken, studentRouter)
// router.use('/', classRouter)
router.use('/schedule',validateToken, scheduleRouter)
router.use('/auth',validateToken, authRouter)

module.exports = router;