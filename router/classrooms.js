const { 
    getAllClassrooms 
} = require('../controller/classrooms')
const router = require('express').Router()

router.get('/classrooms', getAllClassrooms)

module.exports = router