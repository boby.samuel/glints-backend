'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return [
      await queryInterface.addColumn('animals', 'hasWings', {
        type: Sequelize.BOOLEAN
      }),
      await queryInterface.addColumn('animals', 'n_foot', {
        type: Sequelize.INTEGER
      })
    ];
  },

  down: async (queryInterface, Sequelize) => {
    return [
      await queryInterface.removeColumn('animals', 'hasWings'),
      await queryInterface.removeColumn('animals', 'n_foot')
    ];
  }
};
