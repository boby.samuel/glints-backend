const bcrypt = require('bcrypt')

exports.checkPassword = async (saved, test) => {
    try {
        const match = await bcrypt.compare(test, saved)
        return match
    }
    catch(err) {
        throw err
    }
}